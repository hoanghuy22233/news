// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_verify_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterVerifyRequest _$RegisterVerifyRequestFromJson(
    Map<String, dynamic> json) {
  return RegisterVerifyRequest(
    emailOrPhone: json['phone_number'] as String,
    otpCode: json['otpCode'] as String,
  );
}

Map<String, dynamic> _$RegisterVerifyRequestToJson(
        RegisterVerifyRequest instance) =>
    <String, dynamic>{
      'phone_number': instance.emailOrPhone,
      'otpCode': instance.otpCode,
    };
