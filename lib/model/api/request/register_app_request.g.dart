// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterAppRequest _$RegisterAppRequestFromJson(Map<String, dynamic> json) {
  return RegisterAppRequest(
    username: json['username'] as String,
    password: json['password'] as String,
    confirmPassword: json['confirm_password'] as String,
    phoneNumber: json['phone_number'] as String,
    email: json['email'] as String,
  );
}

Map<String, dynamic> _$RegisterAppRequestToJson(RegisterAppRequest instance) =>
    <String, dynamic>{
      'username': instance.username,
      'password': instance.password,
      'confirm_password': instance.confirmPassword,
      'phone_number': instance.phoneNumber,
      'email': instance.email,
    };
