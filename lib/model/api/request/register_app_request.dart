import 'package:json_annotation/json_annotation.dart';

part 'register_app_request.g.dart';

@JsonSerializable()
class RegisterAppRequest {
  @JsonKey(name: "username")
  final String username;
  @JsonKey(name: "password")
  final String password;
  @JsonKey(name: "confirm_password")
  final String confirmPassword;
  @JsonKey(name: "phone_number")
  final String phoneNumber;
  @JsonKey(name: "email")
  final String email;


  RegisterAppRequest({this.username, this.password, this.confirmPassword,
      this.phoneNumber, this.email});

  factory RegisterAppRequest.fromJson(Map<String, dynamic> json) =>
      _$RegisterAppRequestFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterAppRequestToJson(this);
}
