import 'dart:io';

import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/api/request/forgot_password_request.dart';
import 'package:base_code_project/model/api/request/forgot_password_reset_request.dart';
import 'package:base_code_project/model/api/request/forgot_password_verify_request.dart';
import 'package:base_code_project/model/api/request/register_verify_request.dart';
import 'package:base_code_project/model/api/request/resend_otp_request.dart';
import 'package:base_code_project/model/api/response/forgot_password_reset_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_verify_response.dart';
import 'package:base_code_project/model/api/response/register_verify_response.dart';
import 'package:base_code_project/model/api/response/resend_otp_response.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:base_code_project/model/api/request/register_app_request.dart';
import 'package:base_code_project/model/api/response/register_response.dart';

import 'request/barrel_request.dart';
import 'response/barrel_response.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: Endpoint.BASE_URL)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST(Endpoint.LOGIN_APP)
  Future<LoginRegisterResponse> loginApp(
      @Body() LoginAppRequest loginAppRequest);

  @POST(Endpoint.REGISTER_APP)
  Future<RegisterResponse> registerApp(
      @Body() RegisterAppRequest registerAppRequest);

  @POST(Endpoint.REGISTER_VERIFY)
  Future<RegisterVerifyResponse> registerVerify(
      @Body() RegisterVerifyRequest registerVerifyRequest);

  @POST(Endpoint.RESEND_OTP)
  Future<ResendOtpResponse> resendOtp(
      @Body() ResendOtpRequest resendOtpRequest);

  @POST(Endpoint.FORGOT_PASSWORD)
  Future<ForgotPasswordResponse> forgotPassword(
      @Body() ForgotPasswordRequest forgotPasswordRequest);

  @POST(Endpoint.FORGOT_PASSWORD_VERIFY)
  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      @Body() ForgotPasswordVerifyRequest forgotPasswordVerifyRequest);

  @POST(Endpoint.FORGOT_PASSWORD_RESET)
  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      @Body() ForgotPasswordResetRequest forgotPasswordResetRequest);

}
