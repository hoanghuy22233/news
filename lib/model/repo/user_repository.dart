import 'package:base_code_project/model/api/request/barrel_request.dart';
import 'package:base_code_project/model/api/request/forgot_password_request.dart';
import 'package:base_code_project/model/api/request/forgot_password_reset_request.dart';
import 'package:base_code_project/model/api/request/forgot_password_verify_request.dart';
import 'package:base_code_project/model/api/request/register_verify_request.dart';
import 'package:base_code_project/model/api/request/resend_otp_request.dart';
import 'package:base_code_project/model/api/response/barrel_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_reset_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_verify_response.dart';
import 'package:base_code_project/model/api/response/register_verify_response.dart';
import 'package:base_code_project/model/api/response/resend_otp_response.dart';
import 'package:base_code_project/model/api/rest_client.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:base_code_project/model/api/request/register_app_request.dart';
import 'package:base_code_project/model/api/response/register_response.dart';

class UserRepository {
  final Dio dio;

  UserRepository({@required this.dio});

  Future<LoginRegisterResponse> loginApp(
      {@required String username, @required String password}) async {
    final client = RestClient(dio);
    return client
        .loginApp(LoginAppRequest(emailOrPhone: username, password: password));
  }

  Future<RegisterResponse> registerApp(
      {@required String username,
        @required String password,
        @required String confirmPassword,
        @required String phoneNumber,
        @required String email}) async {
    final client = RestClient(dio);
    return client.registerApp(RegisterAppRequest(
      username: username,
      password: password,
      confirmPassword: confirmPassword,
      phoneNumber: phoneNumber,
      email: email,
    ));
  }
  Future<RegisterVerifyResponse> registerVerify(
      {@required String username, @required String otpCode}) async {
    final client = RestClient(dio);
    return client.registerVerify(
        RegisterVerifyRequest(emailOrPhone: username, otpCode: otpCode));
  }
  Future<ResendOtpResponse> resendRegisterOtp(
      {@required String username}) async {
    final client = RestClient(dio);
    return client.resendOtp(ResendOtpRequest(emailOrPhone: username, type: 0));
  }

  Future<ForgotPasswordResponse> forgotPassword(
      {@required String username}) async {
    final client = RestClient(dio);
    return client.forgotPassword(ForgotPasswordRequest(emailOrPhone: username));
  }

  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      {@required String username, @required String otpCode}) async {
    final client = RestClient(dio);
    return client.forgotPasswordVerify(
        ForgotPasswordVerifyRequest(emailOrPhone: username, otpCode: otpCode));
  }

  Future<ResendOtpResponse> resendForgotPasswordOtp(
      {@required String username}) async {
    final client = RestClient(dio);
    return client.resendOtp(ResendOtpRequest(emailOrPhone: username, type: 1));
  }

  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      {@required String username,
        @required String otpCode,
        @required String password,
        @required String confirmPassword}) async {
    final client = RestClient(dio);
    return client.forgotPasswordReset(ForgotPasswordResetRequest(
        emailOrPhone: username,
        otpCode: otpCode,
        password: password,
        confirmPassword: confirmPassword));
  }
}
