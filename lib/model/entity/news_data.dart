import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:equatable/equatable.dart';

part 'news_data.g.dart';

@JsonSerializable()
class NewsData extends Equatable {

  List<News> rows;

  NewsData(this.rows);

  factory NewsData.fromJson(Map<String, dynamic> json) => _$NewsDataFromJson(json);

  Map<String, dynamic> toJson() => _$NewsDataToJson(this);

  @override
  List<Object> get props => [rows];
}
