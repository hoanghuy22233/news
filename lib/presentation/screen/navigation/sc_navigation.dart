import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/account/sc_account.dart';
import 'package:base_code_project/presentation/screen/home/sc_home.dart';
import 'package:base_code_project/presentation/screen/list/sc_list.dart';
import 'package:flutter/material.dart';

class TabNavigatorRoutes {
//  static const String root = '/';
  static const String home = '/home';
  static const String list = '/list';
  static const String account = '/news';
}

class NavigationScreen extends StatefulWidget {
  NavigationScreen();

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  List<FABBottomNavItem> _navMenus = List();
  PageController _pageController;
  int _selectedIndex = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  bool appbar = true;

  // CAN CO DU LIEU ;;

  // _NavigationScreenState({this.type});

  @override
  void initState() {
    _pageController =
        new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        bottomNavigationBar: WidgetFABBottomNav(
          backgroundColor: Colors.red,
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          items: _navMenus,
          selectedColor: Colors.white,
          color: Colors.grey,
        ),
        // drawer: AppDrawer(
        //   drawer: _drawerKey,
        //   menu: goToPage,
        //   notification: goToPage,
        //   account: goToPage,
        // ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [
            HomePage(),
            ListPage(),
            AccountPage(),
          ],
        ));
  }

  List<FABBottomNavItem> _getTab() {
    return List.from([
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/homes.png',),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.list,
          tabItem: TabItem.list,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/img_address_book.png',
      ),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.account,
          tabItem: TabItem.account,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/person.png',
      ),
    ]);
  }

  void goToPage({int page, int id = 0}) {
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
    setState(() {
      this._selectedIndex = page;
    });
    _pageController.jumpToPage(_selectedIndex);
  }
}
