import 'package:base_code_project/presentation/common_widgets/widget_appbar.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetNewsAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbar(
        title: AppLocalizations.of(context).translate('navigation.news'),
        left: [
          Padding(
            padding: EdgeInsets.only(left: 20, top: 20),
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Image.asset(
                "assets/images/left_arrow.png",
                height: 25,
                width: 25,
              ),
            ),
          )
        ],
      ),
    );
  }
}
