import 'package:base_code_project/model/entity/list_new.dart';
import 'package:base_code_project/presentation/screen/detail_new/sc_new_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WigetListNew extends StatelessWidget {
  final int id;
  WigetListNew({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => NewsDetailPage(id: id)),
      ),
      child: Card(
        child: Container(
          width: 200,
          child: Column(
            children: [
              Image.asset(
                "${newList[id].img}",
                fit: BoxFit.cover,
                height: 150,
                width: 200,
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: Container(
                  child: Text(
                    "${newList[id].name}",
                    maxLines: 1,
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 14,
                    ),
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 15),
                child: Text(
                  "${newList[id].content}",
                  style: TextStyle(color: Colors.grey[300], fontSize: 14),
                  textAlign: TextAlign.left,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    newList[id].date,
                    style: TextStyle(color: Colors.grey[300], fontSize: 14),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
