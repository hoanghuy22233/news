
import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/app/constants/string/validator.dart';
import 'package:base_code_project/presentation/common_widgets/widget_login_button.dart';
import 'package:base_code_project/presentation/common_widgets/widget_login_input.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:base_code_project/presentation/screen/register_verify/register_verify_resend/widget_register_verify_resend.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:base_code_project/utils/snackbar/get_snack_bar_utils.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/forgot_password_verify_bloc.dart';
import 'bloc/forgot_password_verify_event.dart';
import 'bloc/forgot_password_verify_state.dart';
import 'forgot_password_verify_resend/widget_forgot_password_verify_resend.dart';

class WidgetForgotPasswordVerifyForm extends StatefulWidget {
  final String username,phone;

  const WidgetForgotPasswordVerifyForm({Key key, @required this.username,@required this.phone})
      : super(key: key);

  @override
  _WidgetForgotPasswordVerifyFormState createState() =>
      _WidgetForgotPasswordVerifyFormState();
}

class _WidgetForgotPasswordVerifyFormState
    extends State<WidgetForgotPasswordVerifyForm> {
  ForgotPasswordVerifyBloc _registerVerifyBloc;

  TextEditingController _otpCodeController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _registerVerifyBloc = BlocProvider.of<ForgotPasswordVerifyBloc>(context);
    _otpCodeController.addListener(_onOtpCodeChange);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordVerifyBloc, ForgotPasswordVerifyState>(
      listener: (context, state) async {
        if (state.isSubmitting) {
          GetSnackBarUtils.createProgress();
        }
        if (state.isSuccess) {
          await GetSnackBarUtils.createSuccess(message: state.message);
          AppNavigator.navigateForgotPasswordReset(
              username: widget.phone, otpCode: _otpCodeController.text);
        }
        if (state.isFailure) {
          GetSnackBarUtils.createError(message: state.message);
        }
      },
      child: BlocBuilder<ForgotPasswordVerifyBloc, ForgotPasswordVerifyState>(
        builder: (context, state) {
          return Padding(
            padding: EdgeInsets.all(20),
            child: Form(
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 6,
                        child: _buildCodeField(),),
                      Expanded(
                          flex: 4,
                          child:  _buildResend())
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Divider(
                      color: Colors.grey,
                      thickness: 1,
                      height: 0,
                    ),
                  ),
                  WidgetSpacer(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: _buildButtonVerify(state),
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool get isPopulated => _otpCodeController.text.isNotEmpty;

  bool isForgotPasswordButtonEnabled() {
    return _registerVerifyBloc.state.isFormValid &&
        isPopulated &&
        !_registerVerifyBloc.state.isSubmitting;
  }

  _buildButtonVerify(ForgotPasswordVerifyState state) {
    return WidgetLoginButton(
      onTap: () {
        if (isForgotPasswordButtonEnabled()) {
          _registerVerifyBloc.add(ForgotPasswordVerifySubmitted(
            username: widget.username,
            otpCode: _otpCodeController.text,
          ));
        }
      },
      isEnable: isForgotPasswordButtonEnabled(),
      text: AppLocalizations.of(context).translate('forgot_password.continue'),
    );
  }

  Widget  _buildCodeField() {
    return WidgetLoginInput(
      inputType: TextInputType.number,
      inputController: _otpCodeController,
      validator: AppValidation.validateUserName(
          AppLocalizations.of(context).translate('register_verify.otpCheck')),
      hint: AppLocalizations.of(context).translate('register_verify.title'),
      leadIcon:  Image.asset(
        'assets/images/envelope.png',
        width: 25,
        height: 25,
        color: Colors.grey,
      ),
    );
  }

  void _onOtpCodeChange() {
    _registerVerifyBloc.add(OtpCodeChanged(otpCode: _otpCodeController.text));
  }

  @override
  void dispose() {
    super.dispose();
  }
  _buildResend() => WidgetForgotPasswordVerifyResend(username: widget.username);
}
