import 'package:base_code_project/model/entity/list_new.dart';
import 'package:base_code_project/presentation/screen/detail_new/widget_new_detail_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewsDetailPage extends StatefulWidget {
  final int id;
  NewsDetailPage({Key key, this.id}) : super(key: key);
  _NewsDetailPageState createState() => new _NewsDetailPageState();
}

class _NewsDetailPageState extends State<NewsDetailPage> {
  bool isReadDetail = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          WidgetNewsDetailAppbar(),
          Expanded(
              child: ListView(
            padding: EdgeInsets.zero,
            scrollDirection: Axis.vertical,
            children: [
              Container(
                child: Image.asset(
                  "${newList[widget.id].img}",
                  height: MediaQuery.of(context).size.height / 2,
                  width: MediaQuery.of(context).size.width,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Text(
                  "${newList[widget.id].name}",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),

              //padding text////
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                child: Container(
                  height: isReadDetail ? null : 50,
                  child: Text(
                    newList[widget.id].content,
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              newList[widget.id].content.length > 100
                  ? Column(
                      children: [
                        Center(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                isReadDetail = !isReadDetail;
                              });
                            },
                            child: isReadDetail
                                ? Text(
                                    "Thu gọn",
                                    style: TextStyle(color: Colors.blue),
                                  )
                                : Text(
                                    "Xem thêm",
                                    style: TextStyle(color: Colors.blue),
                                  ),
                          ),
                        ),
                      ],
                    )
                  : SizedBox(),
            ],
          ))
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
