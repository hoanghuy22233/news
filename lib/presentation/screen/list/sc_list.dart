import 'package:base_code_project/model/entity/news_list.dart';
import 'package:base_code_project/presentation/screen/list/widget_list_item.dart';
import 'package:base_code_project/presentation/screen/list/widget_news_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListPage extends StatelessWidget {
  bool fab = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          WidgetNewsListAppbar(),
          Expanded(
              child: ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: newsList.length,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.symmetric(horizontal: 10),
                child: WidgetPostItem(
                  id: index,
                ),
              );
            },
          ))
        ],
      ),
      floatingActionButton: fab
          ? FloatingActionButton(
              onPressed: () {
                //AppNavigator.navigateCreateDiary();
              },
              tooltip: 'Thêm nhật kí',
              child: Icon(Icons.add),
            )
          : null,
    );
  }
}
