import 'package:base_code_project/model/entity/news_list.dart';
import 'package:base_code_project/presentation/screen/detail_news_list/sc_news_list_detail.dart';
import 'package:flutter/material.dart';

class WidgetPostItem extends StatefulWidget {
  final int id;
  WidgetPostItem({Key key, this.id}) : super(key: key);

  @override
  _WidgetPostItemState createState() => _WidgetPostItemState();
}

class _WidgetPostItemState extends State<WidgetPostItem> {
  bool isReadDetail = false;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => NewsDetailNewsPage(id: widget.id)),
      ),
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Image.asset(
                    'assets/images/logo_remove.png',
                    height: 20,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: Text(
                          newsList[widget.id].name,
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            newsList[widget.id].date + " -",
                            style: TextStyle(
                              fontSize: 10,
                            ),
                            maxLines: 3,
                          ),
                          Image.asset(
                            'assets/images/goldal.jpg',
                            height: 15,
                            width: 15,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            newsList[widget.id].location,
                            style: TextStyle(
                              fontSize: 10,
                            ),
                            maxLines: 3,
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Container(
                      height: isReadDetail ? null : 50,
                      child: Text(
                        newsList[widget.id].content,
                      ),
                    ),
                  ),
                  newsList[widget.id].content.length > 100
                      ? Column(
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    isReadDetail = !isReadDetail;
                                  });
                                },
                                child: isReadDetail
                                    ? Text(
                                        "Thu gọn",
                                        style: TextStyle(color: Colors.blue),
                                      )
                                    : Text(
                                        "Xem thêm",
                                        style: TextStyle(color: Colors.blue),
                                      ),
                              ),
                            ),
                          ],
                        )
                      : SizedBox(),
                  newsList[widget.id].img != null &&
                          newsList[widget.id].img != ""
                      ? Align(
                          alignment: Alignment.centerRight,
                          child: _buildProductImage(),
                        )
                      : Container(),
                ],
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: Divider(
                  color: Colors.grey,
                  height: 10,
                ),
              )
            ],
          ),
          // IconButton(
          //   icon: Icon(
          //     Icons.edit,
          //     size: 15,
          //     color: Colors.blue,
          //   ),
          //   onPressed: () {
          //     AppNavigator.navigateDiaryUpdate(widget.post);
          //   },
          // ),
        ],
      ),
    );
  }

  Widget _buildProductImage() {
    return Container(
      height: 200,
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.asset(
          newsList[widget.id].img,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
