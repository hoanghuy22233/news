import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/widget_circle_progress.dart';
import 'package:base_code_project/presentation/common_widgets/widget_logo.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    openLogin();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height,
            color: Colors.red,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [_buildLogo(), _buildProgress()],
          )
        ],
      ),
    );
  }

  _buildLogo() => WidgetLogo(
        height: 250,
        widthPercent: 250,
      );

  _buildProgress() => WidgetCircleProgress();

  void openLogin() async {
    Future.delayed(Duration(seconds: 3), () {
      AppNavigator.navigateLoginPage();
    });
  }
}
