import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/screen/detail_new/sc_new_detail.dart';
import 'package:base_code_project/presentation/screen/forgot_password/forgot_password_sc.dart';
import 'package:base_code_project/presentation/screen/forgot_password/sc_forgot_password.dart';
import 'package:base_code_project/presentation/screen/forgot_password_reset/forgot_password_reset_page_sc.dart';
import 'package:base_code_project/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:base_code_project/presentation/screen/forgot_password_verify/forgot_password_verify_sc.dart';
import 'package:base_code_project/presentation/screen/forgot_password_verify/sc_forgot_password_verify.dart';
import 'package:base_code_project/presentation/screen/login/login.dart';
import 'package:base_code_project/presentation/screen/login/login_sc.dart';
import 'package:base_code_project/presentation/screen/login_register/login_register.dart';
import 'package:base_code_project/presentation/screen/navigation/sc_navigation.dart';
import 'package:base_code_project/presentation/screen/new/sc_new.dart';
import 'package:base_code_project/presentation/screen/post_news/sc_post_news.dart';
import 'package:base_code_project/presentation/screen/register/sc_register.dart';
import 'package:base_code_project/presentation/screen/register_verify/sc_register_verify.dart';
import 'package:base_code_project/presentation/screen/splash/sc_splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String LOGIN_PAGE = '/login_page';
  static const String LOGIN_REGISTER = '/login_register';
  static const String REGISTER = '/register';
  static const String REGISTER_VERIFY = '/register_verify';
  static const String FORGOT_PASSWORD = '/forgot_password';
  static const String FORGOT_PASSWORD_PAGE = '/forgot_password_page';
  static const String FORGOT_PASSWORD_VERIFY = '/forgot_password_verify';
  static const String FORGOT_PASSWORD_PAGE_VERIFY =
      '/forgot_password_page_verify';
  static const String FORGOT_PASSWORD_RESET = '/forgot_password_reset';
  static const String FORGOT_PASSWORD_RESET_PAGE =
      '/forgot_password_reset_page';
  static const String NEWS = '/news';
  static const String NEWS_DETAIL = '/news_detail';
  static const String NEWS_POST = '/news_post';

  static const String NAVIGATION = '/navigation';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case LOGIN:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case LOGIN_REGISTER:
        return MaterialPageRoute(builder: (_) => LoginOrRegister());
      case LOGIN_PAGE:
        return MaterialPageRoute(builder: (_) => LoginPage());

      case NAVIGATION:
        return MaterialPageRoute(builder: (_) => NavigationScreen());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    var notificationRepository =
        RepositoryProvider.of<NotificationRepository>(context);
    var addressRepository = RepositoryProvider.of<AddressRepository>(context);
    var cartRepository = RepositoryProvider.of<CartRepository>(context);
    var paymentRepository = RepositoryProvider.of<PaymentRepository>(context);
    var invoiceRepository = RepositoryProvider.of<InvoiceRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginScreen(),
      LOGIN_REGISTER: (context) => LoginOrRegister(),
      LOGIN_PAGE: (context) => LoginPage(),
      REGISTER: (context) => RegisterScreen(),
      REGISTER_VERIFY: (context) => RegisterVerifyScreen(),
      FORGOT_PASSWORD: (context) => ForgotPasswordScreen(),
      FORGOT_PASSWORD_VERIFY: (context) => ForgotPasswordVerifyScreen(),
      FORGOT_PASSWORD_RESET: (context) => ForgotPasswordResetScreen(),
      FORGOT_PASSWORD_RESET_PAGE: (context) => ForgotPassResetPage(),
      FORGOT_PASSWORD_PAGE: (context) => ForgotPassPage(),
      FORGOT_PASSWORD_PAGE_VERIFY: (context) => ForgotPassVerifyPage(),
      NAVIGATION: (context) => NavigationScreen(),
      NEWS: (context) => NewsPage(),
      NEWS_DETAIL: (context) => NewsDetailPage(),
      NEWS_POST: (context) => WidgetPostsFormScreen(),
    };
  }
}
