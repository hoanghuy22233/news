import 'package:base_code_project/presentation/router.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AppNavigator {
  AppNavigator._();

  static navigateBack() async {
    Get.back();
  }

  static navigatePopUtil({String name}) async {
    Navigator.popUntil(Get.context, ModalRoute.withName(name));
  }

  static navigateSplash() async {
    var result = await Get.toNamed(BaseRouter.SPLASH);
    return result;
  }

  static navigateLogin() async {
    var result = await Get.toNamed(BaseRouter.LOGIN);
    return result;
  }

  static navigateLoginPage() async {
    var result = await Get.offAllNamed(BaseRouter.LOGIN_PAGE);
    return result;
  }

  static navigateLoginOrRegister() async {
    var result = await Get.toNamed(BaseRouter.LOGIN_REGISTER);
    return result;
  }

  static navigateNavigation() async {
    var result = await Get.offAllNamed(BaseRouter.NAVIGATION);
    return result;
  }

  static navigateRegister() async {
    var result = await Get.toNamed(BaseRouter.REGISTER);
    return result;
  }

  static navigateRegisterVerify({String username, String phone}) async {
    var result = await Get.toNamed(BaseRouter.REGISTER_VERIFY,
        arguments: {'username': username, 'phone': phone});
    return result;
  }

  static navigateForgotPassword() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD);
    return result;
  }

  static navigateForgotPasswordPage() async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD_PAGE);
    return result;
  }

  static navigateForgotPasswordVerify({String username, String phone}) async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD_VERIFY,
        arguments: {'username': username, 'phone': phone});
    return result;
  }

  static navigateForgotPasswordVerifyPage({String username}) async {
    var result = await Get.toNamed(BaseRouter.FORGOT_PASSWORD_PAGE_VERIFY,
        arguments: {'username': username});
    return result;
  }

  static navigateForgotPasswordReset({String username, String otpCode}) async {
    var result =
        await Get.toNamed(BaseRouter.FORGOT_PASSWORD_RESET, arguments: {
      'username': username,
      'otp_code': otpCode,
    });
    return result;
  }

  static navigateForgotPasswordResetPage(
      {String username, String otpCode}) async {
    var result =
        await Get.toNamed(BaseRouter.FORGOT_PASSWORD_RESET_PAGE, arguments: {
      'username': username,
      'otp_code': otpCode,
    });
    return result;
  }

  static navigateNewPage() async {
    var result = await Get.toNamed(BaseRouter.NEWS);
    return result;
  }

  static navigateNewsDetail(int id) async {
    var result =
        await Get.toNamed(BaseRouter.NEWS_DETAIL, arguments: {'id': id});
    return result;
  }
  static navigateNewPost() async {
    var result = await Get.toNamed(BaseRouter.NEWS_POST);
    return result;
  }
}
